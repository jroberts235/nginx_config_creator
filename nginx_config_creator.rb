#!/usr/bin/env ruby
require 'erb'
module ConfigfileCreator

    class RenderConfigFile
        include ERB::Util

        attr_accessor :template
        attr_accessor :sites_dir
        attr_accessor :domain_name
        attr_accessor :filename

        def initialize(sites_dir, domain_name, nginx_conf_d)
            @sites_dir = args[0]
            @domain_name = args[1]
            @filename = "#{args[2]}/#{domain_name}.conf"
            @template = self.get_nginx_template(sites_dir, domain_name)

        end

        def render
             renderer = ERB.new(@template, 0, '-').result(binding)
        end

        def save
            File.open(@filename, "w+") do |f|
                f.write(render)
            end
        end

        def print
            puts render
        end

        def get_nginx_template(sites_dir, domain_name)
          %{
        server {
            listen       80;
            server_name  <%= domain_name %> *.<%= domain_name %>;
            client_max_body_size        20m;
            # access_log   <%= sites_dir %>/<%= domain_name %>/logs/access.log  main;

            location / {
                root   <%= sites_dir %>/<%= domain_name %>/html;
                index  index.php index.html index.htm;
                try_files $uri $uri/ /index.php?q=$uri&$args;
            }

            error_page  404              /404.html;
            location = /404.html {
                root   <%= sites_dir %>/<%= domain_name %>/html;
            }

            error_page   500 502 503 504  /50x.html;
            location = /50x.html {
                root   <%= sites_dir %>/<%= domain_name %>/html;
            }

            location ~ /\.ht {
                deny  all;
            }
        }
          }

        end
    end  # end of class


    if __FILE__ == $0
        begin
            nginx_conf_d = '/etc/nginx/conf.d'
            sites_dir    = '/var/www/sites'
            domain_name = ARGV[0] ||= abort('failed')
            args = ['sites_dir', 'domain_name', 'nginx_conf_d']
            nginx_conf = RenderConfigFile.new(args)
            #nginx_conf.print # print to stdout
            nginx_conf.save()
        rescue => e
            abort 'failed'
        end
    end
end
